import React from 'react';
import Header from '../components/header/Header';
import data from '../data.json';
import photo1 from '../pictures/fn-photo1.jpg';
import photo2 from '../pictures/fn-photo2.jpg';
import photo3 from '../pictures/fn-photo3.jpg';
import photo4 from '../pictures/fn-photo4.jpg';
import photo5 from '../pictures/fn-photo5.jpg';
import photo6 from '../pictures/radiators.jpg';

const Gallery = () => {

    const generateColumnLeft = () => {
        const { gallery } = data;
        const pics = [photo1, photo2, photo3];
        const updatedGallery = gallery.left.map((img, index) => {
            return {
                ...img,
                photo: pics[index]
            }
        })
        return updatedGallery.map((img, index) => {
            return (
                <div key={index}>
                    <img src={img.photo} alt="pic-at-work" />
                    <p>{img.caption}</p>
                </div>
            )
        })
    }
    const generateColumnRight = () => {
        const { gallery } = data;
        const pics = [photo4, photo5, photo6];
        const updatedGallery = gallery.right.map((img, index) => {
            return {
                ...img,
                photo: pics[index]
            }
        })
        return updatedGallery.map((img, index) => {
            return (
                <div key={index}>
                    <img src={img.photo} alt="pic-at-work" />
                    <p>{img.caption}</p>
                </div>
            )
        })
    }

    return (
        <>
            <Header classname={data.headerClassName[1]} />
            <div className="gallery">
                <div>
                    {generateColumnLeft()}
                </div>
                <div>
                    {generateColumnRight()}
                </div>
            </div>
        </>
    )
}
export default Gallery;