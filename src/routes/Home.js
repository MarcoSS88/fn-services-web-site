import React from 'react';
import Header from '../components/header/Header';
import Main from '../components/main/Main';
import data from '../data.json';

const Gallery = () => {

    return (
        <>
            <Header classname={data.headerClassName[0]} />
            <Main />
        </>
    )
}
export default Gallery;