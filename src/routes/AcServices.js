import React from 'react';
import Header from '../components/header/Header';
import data from '../data.json';

const AcServices = () => {

    const generateGrid = () => {
        return data.airConditioningServices.map((table, index) => {
            return (
                <div key={index}>
                    <div>
                        <div>
                            <span className="material-icons">{table.span}</span>
                        </div>
                    </div>
                    <div>
                        <div>
                            <p>{table.title}</p>
                        </div>
                        <div>
                            <p>{table.content}</p>
                        </div>
                    </div>
                </div>
            )
        })
    }

    return (
        <>
            <Header classname={data.headerClassName[2]} />
            <div className="acServices">
                <div>
                    <p>Air Conditioning Services</p>
                </div>
                <div className="gridContainer">
                    {generateGrid()}
                </div>
            </div>
        </>
    )
}
export default AcServices;