import React from 'react';
import About from './About';
import Services from './Services';

const Main = () => {
    return (
        <div className="main">
            <About />
            <Services />
        </div>
    )
}
export default Main;