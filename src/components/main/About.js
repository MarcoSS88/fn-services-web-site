import React from 'react';
import photo from '../../pictures/about-pic.png';

const About = () => {
    return (
        <div className="about">
            <div>
                <img src={photo} alt="setting-airConditioning" />
            </div>
            <div>
                <div>
                    <div className="about-title"><p>About</p></div>
                    <div className="about-content">
                        <p>FN Services offers a range of air conditioning services in many applications, whether is residential, industrial or commercial we got you covered. We also provide small domestic repairs and we do everything with great passion and we can guarantee professional works.</p>
                    </div>
                    <div className="bottom-div">
                        <p>Get in touch</p>
                        <span className="material-icons">ac_unit</span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default About;
