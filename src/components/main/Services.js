import React from 'react';

const Services = () => {
    return (
        <div className="prof-services">
            <div>
                <p>Services</p>
            </div>

            <div>
                <div>
                    <div>
                        <span className="material-icons">build</span>
                    </div>
                    <div>
                        <p>Service And Maintenance</p>
                    </div>
                    <div>
                        <p>We clean and look after your AC systems with care and passion</p>
                    </div>
                </div>

                <div>
                    <div>
                        <span className="material-icons">cleaning_services</span>
                    </div>
                    <div>
                        <p>Filter Caring And Replacement</p>
                    </div>
                    <div>
                        <p>Whatever is your air conditioning system we will find a filter replacement</p>
                    </div>
                </div>

                <div>
                    <div>
                        <span className="material-icons">construction</span>
                    </div>
                    <div>
                        <p>Small Domestic Repairs</p>
                    </div>
                    <div>
                        <p>We can fix and repair electric sockets, switches or fuses and also cover a big range of domestic appliances</p>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Services;