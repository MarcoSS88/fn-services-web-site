import React from 'react';
import data from '../../data.json';
import logo from '../../pictures/logo/fnServices-logoWhite.jpeg';
import { Link } from "react-router-dom";

const Footer = () => {

    const listServices = () => {
        return data.services.map((service, index) => {
            return (
                <p key={index}>{service}</p>
            )
        })
    }
    const listContacts = () => {
        return data.contacts.map((detail, index) => {
            return (
                <div key={index}>
                    <span className="material-icons">{detail.span}</span>
                    <p>{detail.p}</p>
                </div>
            )
        })
    }
    const listBusinessHours = () => {
        return data.businessHours.map((elem, index) => {
            return (
                <p key={index}>{elem}</p>
            )
        })
    }
    const getIcons = () => {
        return data.navIcons.map((icon, index) => {
            if (index === 1) {
                return (
                    <Link to='/acservices' key={index}>
                        <span className="material-icons">{icon}</span>
                    </Link>
                )
            } else return (
                <span key={index} className="material-icons">{icon}</span>
            )
        })
    }

    return (
        <div className="footer">
            <div>
                <div>
                    <div className="companyName">
                        <p>FN Services</p>
                    </div>
                    <div>
                        <p>{data.contentFooter}</p>
                    </div>
                    <div>
                        <img src={logo} alt="company-logo" />
                    </div>
                </div>
                <div>
                    <div>
                        <p>Services</p>
                    </div>
                    <div>
                        {listServices()}
                    </div>
                </div>
                <div>
                    <div>
                        <p>Contact</p>
                    </div>
                    <div>
                        {listContacts()}
                    </div>
                </div>
                <div>
                    <div>
                        <p>Business Hours</p>
                    </div>
                    <div>
                        {listBusinessHours()}
                    </div>
                    <div>
                        {getIcons()}
                    </div>
                </div>
            </div>

            <div>
                <p>{data.bottomInfo}</p>
            </div>
        </div>
    )
}
export default Footer;