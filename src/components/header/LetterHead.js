import React from 'react';

const LetterHead = ({ classname }) => {

    const setLetterHead = () => {
        if (classname === 'header1') {
            return (
                <div>
                    <p>FN Services</p>
                    <p>Air Conditioning & Domestic Services </p>
                </div>
            )
        } else if (classname === 'header2') {
            return (
                <div>
                    <p>Gallery</p>
                    <p>We do everything with great passion <br /> and we can guarantee professional works</p>
                </div>
            )
        } else return (
            <div>
                <p>AC Services</p>
                <p>FN Services offers a range of air conditioning services in many applications</p>
            </div>
        )
    }

    return (
        <div className="letterHead">
            {setLetterHead()}
        </div>
    )
}
export default LetterHead;