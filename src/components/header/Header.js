import React from 'react';
import UpperSection from './UpperSection';
import NavigationBar from './NavigationBar';
import LetterHead from './LetterHead';

const Header = ({ classname }) => {
    return (
        <div className={classname}>
            <UpperSection />
            <NavigationBar />
            <LetterHead classname={classname} />
        </div>
    )
}
export default Header;