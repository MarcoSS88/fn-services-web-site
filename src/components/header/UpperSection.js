import React from 'react';
import logo from '../../pictures/logo/fn-logo.png';

const UpperSection = () => {
    return (
        <div className="upperSection">
            <div>
                <img src={logo} alt="company-logo" />
            </div>
            <div>
                <div className="time-icon">
                    <span className="material-icons">watch_later</span>
                </div>
                <div className="text">
                    <p>Monday - Friday</p>
                    <p>8:00 - 18:00</p>
                </div>
            </div>
        </div>
    )
}
export default UpperSection;