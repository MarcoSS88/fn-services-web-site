import React from 'react';
import data from '../../data.json';
import { Link } from "react-router-dom";

const NavigationBar = () => {

    const buildNavBar = () => {
        const { navigationBar } = data;
        return navigationBar.map((link, index) => {
            if (index === 3) {
                return (
                    <div key={index}>
                        <Link to='/gallery'><p>{link}</p></Link>
                    </div>
                )
            } else if (index === 0) {
                return (
                    <div key={index}>
                        <Link to='/'><p>{link}</p></Link>
                    </div>
                )
            }
            return (
                <div key={index}>
                    <p>{link}</p>
                </div>
            )
        })
    }
    const getIcons = () => {
        const { navIcons } = data;
        return navIcons.map((icon, index) => {
            if (index === 0) {
                return (
                    <div key={index} className={`icon ${icon}`}>
                        <a href="https://www.facebook.com/FN-Services-603254083665241/" target="_blank" rel="noopener noreferrer">
                            <span className="material-icons">{icon}</span>
                        </a>
                    </div>
                )
            } else {
                return (
                    <div key={index} className={`icon ${icon}`}>
                        <Link to='/acservices'>
                            <span className="material-icons">{icon}</span>
                        </Link>
                    </div>
                )
            }
        })
    }

    return (
        <div className="navigationBar">
            {buildNavBar()}
            {getIcons()};
        </div>
    )
}
export default NavigationBar;