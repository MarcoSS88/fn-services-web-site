import React from 'react';
import Layout from './components/Layout';
import Home from './routes/Home';
import AcServices from './routes/AcServices';
import Gallery from './routes/Gallery';
import Footer from './components/footer/Footer';
import { Switch, Route, Redirect } from "react-router-dom";

const App = () => {

  return (
    <Layout>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/acservices" component={AcServices} />
        <Route exact path="/gallery" component={Gallery} />
      </Switch>
      <Footer />
      <Redirect to="/" />
    </Layout>
  )
}
export default App;